<?php
namespace Sixdg\DynamicsCRMConnector\Aspects;

use Go\Aop\Aspect;
use Go\Aop\Intercept\MethodInvocation;
use Go\Lang\Annotation\Around;
use Psr\Log\LoggerInterface;
use Sixdg\DynamicsCRMConnector\Components\TVarDumper;

/**
 * Class ControllerAspect
 *
 * @package Sixdg\SmpAPI\Aspects
 */
class ControllerAspect implements Aspect
{
    private $logger;

    /**
     * @param LoggerInterface $logger
     */
    public function __construct(LoggerInterface $logger)
    {
        $this->logger = $logger;
    }

    /**
     * Method to wrap around all the scrud operations
     *
     * @param MethodInvocation $invocation
     *
     * @Around("execution(public Sixdg\DynamicsCRMConnector\Controllers\DynamicsCRMController->*(*))")
     *
     * @return bool|mixed
     */
    public function aroundMethodExecution(MethodInvocation $invocation)
    {
        $this->logMethodCall($invocation);

        try {
            $result = $invocation->proceed();
        } catch (\Exception $ex) {
            $this->logger->critical("Dynamics CRM query failed " . $ex->getFile() . " " . $ex->getMessage());
            $this->logger->debug($ex->getTraceAsString());
            throw $ex;
        }

        return $result;
    }

    /**
     * @param MethodInvocation $invocation
     */
    private function logMethodCall($invocation)
    {
        $args = $invocation->getArguments();

        $this->logger->info(get_class($invocation->getThis()) . "::" . $invocation->getMethod()->getName());
        $this->logger->debug("called with " . TVarDumper::dump($args, 3));
    }
}
