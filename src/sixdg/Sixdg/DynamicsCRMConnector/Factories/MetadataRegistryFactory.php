<?php
namespace Sixdg\DynamicsCRMConnector\Factories;

use Sixdg\DynamicsCRMConnector\Controllers\DynamicsCRMController;
use Sixdg\DynamicsCRMConnector\Models\Entity;
use Sixdg\DynamicsCRMConnector\Models\EntityMetadataRegistryItem;

/**
 * Class MetadataRegistryFactory
 *
 * @package Sixdg\DynamicsCRMConnector\Factories
 */
class MetadataRegistryFactory
{
    private $serialization = 'http://schemas.microsoft.com/2003/10/Serialization/Arrays';

    private $entity;
    private $controller;
    private $metadataRegistry;
    private $cache = null;

    /**
     * @param DynamicsCRMController $controller
     */
    public function __construct(DynamicsCRMController $controller = null)
    {
        if ($controller) {
            $this->controller = $controller;
            $this->cache = $controller->getCache();
        }
    }

    /**
     * @param Entity $entity
     *
     * @return mixed
     */
    public function makeMetaData($entity)
    {
        $metaData = null;
        $this->entity = $entity;
        $cacheKey = $this->getCacheKey($entity->getEntityName());

        if (isset($this->metadataRegistry[$cacheKey])) {
            return $this->metadataRegistry[$cacheKey];
        }

        //external caching system
        if ($this->cache) {
            $metaData = $this->getMetaDataFromExternalCache($cacheKey);
        }

        if (!$metaData) {
            $metaData = $this->fetchMetaData();
            if ($this->cache) {
                $this->cache->set($cacheKey, serialize($metaData), 3600);
            }
        }

        $this->metadataRegistry[$cacheKey] = $metaData;

        return $this->metadataRegistry[$cacheKey];
    }

    /**
     * @param $entityName
     *
     * @return string
     */
    private function getCacheKey($entityName)
    {
        $key = $entityName . "MetaData";
        if ($this->cache) {
            $key = $this->cache->getKeyPrefix() . '-' . $key;
        }

        return $key;
    }

    /**
     * @param $cacheKey
     *
     * @return mixed|null
     */
    private function getMetaDataFromExternalCache($cacheKey)
    {
        if ($this->cache->exists($cacheKey)) {
            return unserialize($this->cache->get($cacheKey));
        }

        return null;
    }

    /**
     * @param string $xml
     *
     * @return mixed
     */
    public function fetchMetaData($xml = null)
    {
        $metadataRegistry = [];

        if (!$xml) {
            $xml = $this->controller->findMetadata($this->entity);
            if (!$xml) {
                throw new \RuntimeException(
                    "Unable to find meta data for " . get_class($this->entity) . " check logfile"
                );
            }
        }

        $attributes = $this->findAttributesFromXML($xml);

        foreach ($attributes as $attribute) {

            $properties = $attribute->children('d', true);

            $item = new EntityMetadataRegistryItem();
            $item->setLabel((string) $properties->DisplayName->children('b', true)->UserLocalizedLabel->Label);
            $item->setDescription((string) $properties->Description->children('b', true)->UserLocalizedLabel->Label);
            $item->setRequiredLevel((string) $properties->RequiredLevel->children('b', true)->Value, $item);
            $this->setBooleanProperties($properties, $item);
            $this->setIsLookup($attribute, $item);
            $item->setAttributeType((string) $properties->AttributeType);

            if ($item->getIsLookup()) {
                $this->setLookupTypes($properties, $item);
            }

            if (isset($properties->OptionSet)) {
                $item->setOptionSet((string) $properties->OptionSet->OptionSetType, $item);
            }

            $metadataRegistry[(string) $properties->LogicalName] = $item;
        }

        return $metadataRegistry;
    }

    /**
     * @param string $xml
     *
     * @return \SimpleXMLElement[]
     * @throws \RuntimeException
     */
    private function findAttributesFromXML($xml)
    {
        $metaDataPath = '/s:Envelope/s:Body/dft:ExecuteResponse/dft:ExecuteResult/b:Results'
            . '/b:KeyValuePairOfstringanyType/c:value[@i:type="d:EntityMetadata"]/d:Attributes/d:AttributeMetadata';

        $dom = simplexml_load_string($xml);
        if ($dom === false) {
            throw new \RuntimeException("Unable to load xml with simple_xml check log file.");
        }

        $dom->registerXPathNamespace('s', 'http://www.w3.org/2003/06/soap-envelope');
        $dom->registerXPathNamespace('b', 'http://schemas.microsoft.com/xrm/2011/Contracts');
        $dom->registerXPathNamespace('c', 'http://schemas.datacontract.org/2004/07/System.Collections.Generic');
        $dom->registerXPathNamespace('d', 'http://schemas.microsoft.com/xrm/2011/Metadata');
        $dom->registerXPathNamespace('i', 'http://www.w3.org/2001/XMLSchema-instance');
        $dom->registerXPathNamespace('dft', 'http://schemas.microsoft.com/xrm/2011/Contracts/Services');

        $nodeList = $dom->xpath($metaDataPath);
        if ($nodeList === false) {
            throw new \RuntimeException("Invalid xml from CRM");
        }

        return $nodeList;
    }

    /**
     * @param object $properties
     * @param object $item
     */
    private function setBooleanProperties($properties, $item)
    {
        $matchingKeys = [
            'IsCustomAttribute',
            'IsPrimaryId',
            'IsPrimaryName',
            'IsValidForCreate',
            'IsValidForUpdate',
            'IsValidForRead',
        ];

        foreach ($matchingKeys as $key) {
            $itemKey = "set" . $key;
            $item->$itemKey(filter_var($properties->$key, FILTER_VALIDATE_BOOLEAN));
        }
    }

    /**
     * @param object $attribute
     * @param object $item
     */
    private function setIsLookup($attribute, $item)
    {
        $item->setIsLookup(false);

        $attributeList = $attribute->attributes('http://www.w3.org/2001/XMLSchema-instance');
        $attributeType = $this->stripNS($attributeList['type']);
        if ($attributeType === 'LookupAttributeMetadata') {
            $item->setIsLookup(true);
        }
    }

    /**
     * @param object $attribute
     * @param object $item
     */
    private function setLookupTypes($attribute, $item)
    {
        $types = [];
        foreach ($attribute->Targets->children($this->serialization) as $target) {
            $types[] = (string) $target;
        }
        $item->setLookupTypes($types);
    }

    /**
     * Utility function to strip any Namespace from an XML attribute value
     * @param  String $attributeValue
     * @return String Attribute Value without the Namespace
     */
    protected function stripNS($attributeValue)
    {
        return preg_replace('/[a-zA-Z]+:([a-zA-Z]+)/', '$1', $attributeValue);
    }
}
