<?php
/**
 * Created by JetBrains PhpStorm.
 * User: alan.hollis
 * Date: 07/08/13
 * Time: 11:03
 */
namespace Sixdg\DynamicsCRMConnector\AspectKernel;

use Go\Core\AspectContainer;
use Go\Core\AspectKernel;

class ApplicationAspectKernel extends AspectKernel
{
    protected function configureAop(AspectContainer $container)
    {

    }

    protected function getApplicationLoaderPath()
    {
    }

    public function registerAspect(Aspect $aspect)
    {
        $this->getContainer()->registerAspect($aspect);
    }
}
