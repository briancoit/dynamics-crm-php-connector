<?php

namespace Sixdg\DynamicsCRMConnector\Responses;

/**
 * Class RetrieveMultipleResponse
 *
 * @package Sixdg\DynamicsCRMConnector\Responses
 */
class RetrieveMultipleResponse extends DynamicsCRMResponse
{

    protected $multipleResultNode;

    /**
     *
     * @param  type      $source
     * @param  type      $options
     * @return boolean
     * @throws Exception
     */
    public function loadXML($source, $options = 0)
    {
        $parentResult = parent::loadXML($source, $options);
        $retrieveMultipleResponseNode = null;
        foreach ($this->getElementsByTagName('RetrieveMultipleResponse') as $node) {
            $retrieveMultipleResponseNode = $node;
            break;
        }
        if ($retrieveMultipleResponseNode == null) {
            throw new Exception('Could not find RetrieveMultipleResponse node in XML provided');

            return false;
        }

        return $parentResult;
    }

    /**
     * Find the PagingCookie
     *
     * @returns string | DOMElement | null
     */
    public function getPagingCookie($toSting = true)
    {
        $pagingCookie = null;
        if ($this->getElementsByTagName('PagingCookie')->length !== 0) {
            $pagingCookie = $this->getElementsByTagName('PagingCookie')->item(0);
        }
        if ($pagingCookie and $toSting) {
            return $pagingCookie->textContent;
        }

        return $pagingCookie;
    }

    /**
     * Find the PageNumber in the PagingCookie
     *
     * @returns int
     */
    public function getPageNumber()
    {
        $pagingDOM = new \DOMDocument();
        $pagingCookie = $this->getPagingCookie(true);
        if (!$pagingCookie) {
            return null;
        }
        $pagingDOM->loadXML($pagingCookie);
        /* Find the page number */
        $pageNo = $pagingDOM->documentElement->getAttribute('page');

        return (int) $pageNo;
    }

    /**
     *
     * @return DOMNode
     * @throws Exception
     */
    protected function getRetrieveMultipleResultNode()
    {
        if (!$this->multipleResultNode) {
            foreach ($this->getElementsByTagName('RetrieveMultipleResult') as $node) {
                $this->multipleResultNode = $node;
                break;
            }
            if ($this->multipleResultNode == null) {
                throw new Exception('Could not find RetrieveMultipleResult node in XML provided');

                return false;
            }
        }

        return $this->multipleResultNode;
    }

    /**
     *  returns the more records tag value
     *
     * @return string
     */
    public function getMoreRecords()
    {
        return $this->getTagValue('MoreRecords');
    }

    /**
     *  returns the value for a given tag
     *
     * @return string
     */
    protected function getTagValue($tagName)
    {
        if ($this->getRetrieveMultipleResultNode()->getElementsByTagName($tagName)->length == 0) {
            return null;
        }

        return $this->getRetrieveMultipleResultNode()->getElementsByTagName($tagName)->item(0)->textContent;
    }

    /**
     * @inherit
     */
    public function asArray()
    {
        $entities = array();
        foreach ($this->getRetrieveMultipleResultNode()->getElementsByTagName('Entities')->item(0)->getElementsByTagName('Entity') as $entityNode) {
            $entityArray = $this->getArrayFromNamespace(
                'http://schemas.datacontract.org/2004/07/System.Collections.Generic',
                false,
                $entityNode
            );
            $entityArray['id'] = $this->getEntityId($entityNode);
            $entities[] = $entityArray;
        }

        return $entities;
    }

    private function getEntityId($node)
    {
        $tags = $node->getElementsByTagName('Id');
        $idTag = $tags->item($tags->length - 1);

        return $idTag->textContent;
    }
}
