<?php

use Sixdg\DynamicsCRMConnector\Test\BaseTest;

/**
 * Created by JetBrains PhpStorm.
 * User: alan.hollis
 * Date: 24/07/13
 * Time: 13:43
 */

class EntityTest extends BaseTest
{
    protected $entity;

    public function setUp()
    {
        $entityFactory = $this->getEntityFactory('contact');
        $this->entity = $entityFactory->makeEntity('contact');
    }

    /**
     * @expectedException InvalidArgumentException
     */
    public function testSetOfNonMetaItemThrowsException()
    {
        $this->entity->made_up_logical_name = "test";
    }

    public function testSetValidMetaDataSetsData()
    {
        $city = $GLOBALS['faker']->city;

        $this->entity->address1_city = $city;
        $this->assertEquals($city, $this->entity->address1_city);
    }

    public function testMagicMethods()
    {
        $this->entity->setaddress1_city('anyvalue');
        $this->assertEquals($this->entity->getaddress1_city(), 'anyvalue');
        $this->assertTrue(isset($this->entity->address1_city), 'isset method failing');
        unset($this->entity->address1_city);
        $this->assertFalse(isset($this->entity->address1_city), 'unset method failing');
    }

    public function testSetMagicMethodThrowsExceptionIfInvalid()
    {
        $this->setExpectedException('InvalidArgumentException');
        $this->entity->setMethodThatWillNeverExist('thing');
    }

    public function testGetMagicMethodThrowsExceptionIfInvalid()
    {
        $this->setExpectedException('InvalidArgumentException');
        $this->entity->getMethodThatWillNeverExist('thing');
    }

    public function testSetAndGetLinkEntity()
    {
        $clone = clone($this->entity);
        //the key should be always lowercased so we test with some capital letters
        $this->entity->setLinkEntity('cLonE', $clone);
        $returnedEntity = $this->entity->getClone();
        $this->assertEquals($clone, $returnedEntity);
    }
}
