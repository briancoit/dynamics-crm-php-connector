<?php
namespace Sixdg\DynamicsCRMConnector\Components\DOM;

use Sixdg\DynamicsCRMConnector\Components\DOM\DOMHelper;

/**
 * Created by JetBrains PhpStorm.
 * User: alan.hollis
 * Date: 04/07/13
 * Time: 09:33
 */
class DOMHelperTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @var DOMHelper
     */
    protected $domHelper;

    public function setUp()
    {
        $this->domHelper = new DOMHelper();
    }

    public function testCreateDocumentFragment()
    {
        $element = $this->domHelper->createDocumentFragment();
        $this->assertTrue($element instanceof \DOMDocumentFragment);
    }

    public function testCreateElement()
    {
        $element = $this->domHelper->createElement('test', 'value');
        $this->assertTrue($element instanceof \DOMElement);
    }

    public function testCreateElementNS()
    {
        $element = $this->domHelper->createElementNS('testns', 'test');
        $this->assertTrue($element instanceof \DOMElement);
    }
}
